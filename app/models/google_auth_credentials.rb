require 'singleton'

class GoogleAuthCredentials
  include Singleton

  def initialize
    @auth_instance = Google::Auth::UserRefreshCredentials.new(additional_parameters: {
        "include_granted_scopes" => "true",
        "prompt" => "consent"
      },
      client_id: "571945631403-ddvmgutmjcshtcrj8i1e58vmi9oobarn.apps.googleusercontent.com",
      client_secret: "YDUtQi_YY5E9wJO1BeWWxKdm",
      scope: [
        "https://www.googleapis.com/auth/drive",
        "https://spreadsheets.google.com/feeds/",
      ],
      redirect_uri: "http://localhost:3000/teachers/google_sessions/new"
    )
  end

  def auth_credentials
    @auth_instance
  end

  def refresh_token
    @auth_instance.refresh_token
  end
end
