module Teachers
  class GoogleSessionsController < BaseController
    skip_before_action :check_google_client

    def new
      google_auth_credentials.code = params[:code]
      google_auth_credentials.fetch_access_token!
      session[:refresh_token] = GoogleAuthCredentials.instance.refresh_token
      redirect_to teachers_classrooms_path
    end
  end
end
