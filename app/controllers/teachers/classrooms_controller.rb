module Teachers
  class ClassroomsController < BaseController
    before_action :set_classroom, only: [:edit, :create, :destroy]

    def index
      @classrooms = Classroom.all
    end

    def new
      @classroom = Classroom.new
    end

    def edit
    end

    def update

    end

    def create
      @classroom = ClassroomDecorator.new(@classroom).create(permitted_attributes(Classroom))

      if @classroom.save
        redirect_to teacher_classrooms_path
      else
        render :new
      end
    end

    def destroy

    end

    private

    def set_classroom
      @classroom = Classroom.find(params[:id])
    end
  end
end
