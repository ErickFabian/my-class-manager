module Teachers
  class BaseController < ApplicationController
    before_action :check_google_client, :google_session

    private

    def check_google_client
      Rails.logger.info(session[:refresh_token])
      if !session[:refresh_token]
        redirect_to google_auth_authorization_uri
      else
        google_auth_credentials.refresh_token = session[:refresh_token]
        #google_auth_credentials.fetch_access_token!
      end
    end


    def google_session
      @google_session ||=
        GoogleDrive::Session.from_credentials(google_auth_credentials)
    end

    def google_auth_credentials
      GoogleAuthCredentials.instance.auth_credentials
    end

    def google_auth_authorization_uri
      GoogleAuthCredentials.instance.auth_credentials.authorization_uri.to_s
    end
  end
end
