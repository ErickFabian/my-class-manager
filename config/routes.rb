Rails.application.routes.draw do
  devise_for :teachers

  namespace :teachers do
    resources :classrooms
    resources :google_sessions
  end

  root to: "dashboards#index"
end
