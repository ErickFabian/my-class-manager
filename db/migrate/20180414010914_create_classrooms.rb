# frozen_string_literal: true

class CreateClassrooms < ActiveRecord::Migration[5.2]
  def change
    create_table :classrooms do |t|
      t.string :google_id
      t.string :name

      t.timestamps
    end
  end
end
